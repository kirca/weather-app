package com.example.weatherapp.ui.main.view;

import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;


import com.example.weatherapp.Constants;
import com.example.weatherapp.R;
import com.example.weatherapp.data.Result;
import com.example.weatherapp.data.api.model.response.WeatherModel;
import com.example.weatherapp.databinding.ActivityMainBinding;
import com.example.weatherapp.ui.main.viewmodel.WeatherViewModel;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.squareup.picasso.Picasso;


import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.example.weatherapp.data.Result.*;
import static com.example.weatherapp.data.Result.Error;

public class HomeActivity extends BaseLocationActivity {


    private static final String TAG = "home" ;
    private AutocompleteSupportFragment autocompleteFragment;
    private WeatherViewModel weatherViewModel;
    private ActivityMainBinding mainBinding;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);

        weatherViewModel =  new ViewModelProvider(this).get(WeatherViewModel.class);



        setupPlaceAutoComplete();
        checkSettingsAndAcquireLocation();


    }


    private void getWeatherData(Map<String, String> data) {

        weatherViewModel.getWeatherData(data).observe(this, new Observer<Result<WeatherModel>>() {
            @Override
            public void onChanged(Result<WeatherModel> weatherModelResult) {

                if (weatherModelResult instanceof Success){
                     Success result = (Success)weatherModelResult;
                     WeatherModel model = (WeatherModel) result .data;
                     updateUi(model);

                }else{
                    Error result = (Error)weatherModelResult;
                    Toast.makeText(HomeActivity.this,result.exception.getMessage(),Toast.LENGTH_SHORT).show();


                }



            }
        });

    }

    private void updateUi(WeatherModel model) {
        autocompleteFragment.setText(model.getName());


        mainBinding.tvTempMinValue.setText(getString(R.string.temp_value,(int)model.getMain().getTempMin()));
        mainBinding.tvTempMaxValue.setText(getString(R.string.temp_value,(int)model.getMain().getTempMax()));
        mainBinding.tvPressureValue.setText(getString(R.string.pressure_value,model.getMain().getPressure()));
        mainBinding.tvHumidityValue.setText(getString(R.string.humidity_value,model.getMain().getHumidity()));

        mainBinding.tvCityName.setText(model.getName());
        mainBinding.tvMainTemp.setText(getString(R.string.temp_value,(int)model.getMain().getTemp()));

        mainBinding.tvDesc.setText(model.getWeather().get(0).getDescription());
        mainBinding.tvWeatherMain.setText(model.getWeather().get(0).getMain());
        mainBinding.tvtFeelTemp.setText(getString(R.string.temp_feels_like,(int)model.getMain().getFeelsLike()));

        Picasso.get().load(getString(R.string.icon_url,model.getWeather().get(0).getIcon())).fit().centerInside().into(mainBinding.ivWeatherIcon);

    }

    private void setupPlaceAutoComplete() {

        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        autocompleteFragment.setTypeFilter(TypeFilter.CITIES);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                getWeatherData(getCityQueryData(place.getName()));
            }

            @Override
            public void onError(@NotNull Status status) {
                Toast.makeText(HomeActivity.this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    void onLocationAcquired(Location location) {
        onLocationUpdate(location);

    }


    public void onLocationUpdate(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());

        getWeatherData(getLocationQueryData(location.getLatitude(), location.getLongitude()));

    }

    private Map<String, String> getLocationQueryData(double latitude, double longitude) {
        Map<String, String> data = new HashMap<>();
        data.put("lat", String.valueOf(latitude));
        data.put("lon", String.valueOf(longitude));
        data.put("units", Constants.METRIC_UNITS);
        data.put("appid", getString(R.string.weather_api_key));

        return data;
    }

    private Map<String, String> getCityQueryData(String cityName) {
        Map<String, String> data = new HashMap<>();
        data.put("q", cityName);
        data.put("units",Constants.METRIC_UNITS);
        data.put("appid", getString(R.string.weather_api_key));
        return data;
    }




}