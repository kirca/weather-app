package com.example.weatherapp.ui.main.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.weatherapp.data.Result;
import com.example.weatherapp.data.api.model.response.WeatherModel;
import com.example.weatherapp.data.repo.WeatherDataRepository;

import java.util.Map;

public class WeatherViewModel extends ViewModel {

    private WeatherDataRepository weatherRepo;
    private MutableLiveData<Result<WeatherModel>> mutableLiveData;

    public WeatherViewModel() {
        weatherRepo = new WeatherDataRepository();
    }

    public LiveData<Result<WeatherModel>> getWeatherData(Map<String, String> data) {

        mutableLiveData = weatherRepo.requestWeather(data);

        return mutableLiveData;
    }


}
