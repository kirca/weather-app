package com.example.weatherapp;

import android.app.Application;

import com.example.weatherapp.data.api.RestClient;
import com.google.android.libraries.places.api.Places;

public class WeatherApplication extends Application {



    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();
        initRestClient();
        initPlacesSdk();
    }


    public   void initRestClient(){

        restClient = new RestClient(Constants.BASE_URL);
    }

    private void initPlacesSdk(){

        Places.initialize(getApplicationContext(), getString(R.string.google_api_key));
    }

    public static RestClient getRestClient() {
        return restClient;
    }

}
