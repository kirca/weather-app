package com.example.weatherapp;

public class Constants {



    public static final String BASE_URL = "https://api.openweathermap.org";
    public static final int LOCATION_REQUEST =100 ;
    public static final int REQUEST_CHECK_SETTINGS = 101  ;
    public static final String METRIC_UNITS = "metric";

}
