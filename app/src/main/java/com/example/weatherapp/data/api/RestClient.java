package com.example.weatherapp.data.api;

import com.example.weatherapp.data.api.service.UrlService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    
    private UrlService apiService;
    private Retrofit retrofit;

    public RestClient(String baseUrl) {
        init(baseUrl);
    }
    
    private void init(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHttpClient())
                .build();
        this.retrofit = retrofit;
        apiService = retrofit.create(UrlService.class);
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return builder.build();
    }

    public UrlService getUrlService() {
        return apiService;
    }
    public Retrofit getRetrofit() {
        return retrofit;
    }



}
