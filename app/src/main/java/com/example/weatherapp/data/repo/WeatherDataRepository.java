package com.example.weatherapp.data.repo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.weatherapp.WeatherApplication;
import com.example.weatherapp.data.Result;
import com.example.weatherapp.data.api.RestClient;
import com.example.weatherapp.data.api.model.response.WeatherModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDataRepository {

    private final String TAG = getClass().getSimpleName();


    public MutableLiveData<Result<WeatherModel>> requestWeather(Map<String, String> data) {
        final MutableLiveData<Result<WeatherModel>> mutableLiveData = new MutableLiveData<>();

        RestClient restClient =
                WeatherApplication.getRestClient();

        restClient.getUrlService().getWeather(data).enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                Log.d(TAG, "getCurrencyList response=" + response);

                if (response.isSuccessful() && response.body() != null) {
                    WeatherModel weatherResponse = response.body();
                    mutableLiveData.setValue(new Result.Success<WeatherModel>(weatherResponse));

                }
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                Log.d(TAG, " onFailure" + call.toString());
                mutableLiveData.setValue(new Result.Error<WeatherModel>(t));
            }
        });

        return mutableLiveData;
    }




}
