package com.example.weatherapp.data.api.service;

import com.example.weatherapp.data.api.model.response.WeatherModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface UrlService {




    @GET("/data/2.5/onecall?")
    Call<Response> getWeatherInfo(@QueryMap Map<String, String> options);



    @GET("/data/2.5/weather?")
    Call<WeatherModel> getWeather(@QueryMap Map<String, String> options);






}
